Distilled chemical databases derived from publicly available compound databases for use with ChemDistiller (part of Metaspace).

This is the default folder where ChemDistiller would look for chemical databases unless overriden with the command line option. 
ChemDistiller will load them when started.

These are not the original compound databases from the Internet, but the re-processed ones. Only the original SMILES and compound IDs were used from the source databases.Fingerprints, Fragprints, InChi strings etc. were generated using either OpenBabel/Pybel or our scripts (see ChemDistiller documentation for details). 

For detailed information about licensing of the source databases please see corresponding websites:

Database	Description	Compound Count	Source URL
ChEBI 	 	Chemical Entities of Biological Interest	81,753	https://www.ebi.ac.uk/chebi/
ECMDB 	 	E. coli metabolome	3,730	http://ecmdb.ca/
FooDB 	 	Food constituents	22,763	http://foodb.ca/
HMDB 	 	Human metabolome	41,758	http://www.hmdb.ca/
MassBank 	HQ Mass spectra	11,865	http://www.massbank.jp/?lang=en
SMPDB	 	Small molecule pathway database	4,297	http://smpdb.ca/
YMDB 	 	Yeast metabolome	1,997	http://www.ymdb.ca/

Currently these databases are for internal use by the Metaspace consortium only before the corresponding paper is published.
